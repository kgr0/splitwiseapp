﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SplitwiseApplication.Domain.Interfaces
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
