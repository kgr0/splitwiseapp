﻿using SplitwiseApplication.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace SplitwiseApplication.Domain.Models
{
    public class ExpenseHeader : IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }

        public int UserId { get; set; }
        public int GroupId { get; set; }

        public ICollection<ExpenseLine> ExpenseLines { get; set; }
        public User User { get; set; }
        public Group Group { get; set; }
    }
}
