﻿using SplitwiseApplication.Domain.Interfaces;

namespace SplitwiseApplication.Domain.Models
{
    public class ExpenseLine : IEntity<int>
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }

        public int ExpenseId { get; set; }
        public int ForUserId { get; set; }

        public ExpenseHeader ExpenseHeader { get; set; }
        public User User { get; set; }
    }
}
