﻿using SplitwiseApplication.Domain.Interfaces;
using System.Collections.Generic;

namespace SplitwiseApplication.Domain.Models
{
    public class Group : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Payment> Payments { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
    }
}
