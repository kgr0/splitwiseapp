﻿using SplitwiseApplication.Domain.Interfaces;
using System;

namespace SplitwiseApplication.Domain.Models
{
    public class Payment : IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
        public bool IsConfirmed { get; set; }

        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public int GroupId { get; set; }

        public User FromUser { get; set; }
        public User ToUser { get; set; }
        public Group Group { get; set; }
    }
}
