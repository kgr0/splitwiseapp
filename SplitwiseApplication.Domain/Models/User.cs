﻿using SplitwiseApplication.Domain.Interfaces;
using System.Collections.Generic;

namespace SplitwiseApplication.Domain.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public ICollection<Payment> PaymentsFromUser { get; set; }
        public ICollection<Payment> PaymentsToUser { get; set; }
        public ICollection<Group> Groups { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public ICollection<ExpenseLine> ExpenseLines { get; set; }
    }
}
