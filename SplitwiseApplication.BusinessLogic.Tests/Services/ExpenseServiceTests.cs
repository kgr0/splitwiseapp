﻿using Ardalis.Specification;
using Moq;
using SplitwiseApplication.BusinessLogic.Exceptions;
using SplitwiseApplication.BusinessLogic.Models;
using SplitwiseApplication.BusinessLogic.Services.Implementation;
using SplitwiseApplication.BusinessLogic.Specifications;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SplitwiseApplication.BusinessLogic.Tests.Services
{
    public class ExpenseServiceTests
    {
        private readonly Mock<IGroupRepository> _groupRepositoryMock;
        private readonly Mock<IExpenseHeaderRepository> _expenseRepositoryMock;
        private readonly Mock<IPaymentRepository> _paymentRepositoryMock;
        private ExpenseService _service;

        public ExpenseServiceTests()
        {
            _groupRepositoryMock = new Mock<IGroupRepository>();
            _expenseRepositoryMock = new Mock<IExpenseHeaderRepository>();
            _paymentRepositoryMock = new Mock<IPaymentRepository>();
            _service = new ExpenseService(_expenseRepositoryMock.Object, _groupRepositoryMock.Object, _paymentRepositoryMock.Object);
        }

        [Fact]
        public void CalculateDebtAsync_GroupDoesNotExist_GroupNotFoundExceptionIsThrown()
        {
            //Arrange
            _groupRepositoryMock.Setup(x => x.GetSingleAsync(new GetUsersOfGroupSpecification(0)))
                                .Returns(Task.FromResult((Group)null));

            //Act
            //Assert
            Assert.ThrowsAsync<GroupNotFoundException>(async () => await _service.CalculateDebtAsync(0, 0, 1));
        }

        [Fact]
        public void CalculateDebtAsync_UsersNotInGroup_UserNotInGroupExceptionIsThrown()
        {
            //Arrange
            var group = new Group
            {
                Id = 0,
                Name = "MyGroup"
            };
            SetupOnlyGroupGetDebt(group);

            //Act
            //Assert
            Assert.ThrowsAsync<UserNotInGroupException>(async () => await _service.CalculateDebtAsync(0, 0, 1));
        }
        [Fact]
        public async void CalculateDebtAsync_NoExpensesNoPaymentsBetweenUsers_DebtEqualsZero()
        {
            //Arrange
            var group = InitGroup();
            SetupOnlyGroupGetDebt(group);
            var expected = InitDebt(0);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }
        [Fact]
        public async void CalculateDebtAsync_ExpensesAndUnconfirmedPaymentsOnlyFromFirstUser_Debt()
        {
            //Arrange
            Group group = InitGroup_ExpensesAndUnconfirmedPaymentsOnlyFromFirstUser();
            SetupGetDebt(group);
            var expected = InitDebt(80);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }
        [Fact]
        public async void CalculateDebtAsync_ExpensesAndPaymentsOnlyFromFirstUser_Debt_20()
        {
            //Arrange
            Group group = InitGroup_ExpensesAndPaymentsOnlyFromFirstUser();
            SetupGetDebt(group);
            var expected = InitDebt(-20);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }
        [Fact]
        public async void CalculateDebtAsync_FirstUserHasDebt_Debt10()
        {
            //Arrange
            Group group = InitGroup_FirstUserHasDebt();
            SetupGetDebt(group);
            var expected = InitDebt(10);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }
        [Fact]
        public async void CalculateDebtAsync_NoOneHasDebt_DebtEqualsZero()
        {
            //Arrange
            Group group = InitGroup_NoOneHasDebt();
            SetupGetDebt(group);
            var expected = InitDebt(0);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }
        [Fact]
        public async void CalculateDebtAsync_SecondUserHasDebt_Debt_20()
        {
            //Arrange
            Group group = InitGroup_SecondUserHasDebt();
            SetupGetDebt(group);
            var expected = InitDebt(-20);

            //Act
            var result = (await _service.CalculateDebtAsync(0, 0, 1));

            //Assert
            Assert.True(AreTwoDebtEqual(expected, result));
        }

        private bool AreTwoDebtEqual(Debt first, Debt second)
        {
            return first.Amount == second.Amount
                && first.FromUserId == second.FromUserId
                && second.ToUserId == second.ToUserId;
        }
        private void SetupGetDebt(Group group)
        {
            _groupRepositoryMock.Setup(x => x.GetSingleAsync(It.IsAny<Specification<Group>>()))
                           .Returns(Task.FromResult(group));

            IEnumerable<ExpenseHeader> expensesList = group.ExpenseHeaders.Select(x => x);
            _expenseRepositoryMock.Setup(x => x.GetManyAsync(It.IsAny<Specification<ExpenseHeader>>()))
                .Returns(Task.FromResult(expensesList));

            IEnumerable<Payment> paymentsList = group.Payments.Where(x => x.GroupId == 0
                    && (x.FromUserId == 0 || x.ToUserId == 0)
                    && x.IsConfirmed);
            _paymentRepositoryMock.Setup(x => x.GetManyAsync(It.IsAny<Specification<Payment>>()))
              .Returns(Task.FromResult(paymentsList));

            _service = new ExpenseService(_expenseRepositoryMock.Object, _groupRepositoryMock.Object, _paymentRepositoryMock.Object);
        }
        private void SetupOnlyGroupGetDebt(Group group)
        {
            _groupRepositoryMock.Setup(x => x.GetSingleAsync(It.IsAny<Specification<Group>>()))
                           .Returns(Task.FromResult(group));
            _service = new ExpenseService(_expenseRepositoryMock.Object, _groupRepositoryMock.Object, _paymentRepositoryMock.Object);
        }
        private Group InitGroup()
        {
            return new Group
            {
                Id = 0,
                Name = "MyGroup",
                Users = new List<User>
                {
                    new User
                    {
                        Id = 0,
                        Name = "Bob"
                    },
                     new User
                    {
                        Id = 1,
                        Name = "Mary"
                    }
                }
            };
        }
        private Debt InitDebt(decimal amount)
        {
            return new Debt
            {
                FromUserId = 0,
                ToUserId = 1,
                Amount = amount
            };
        }

        private Group InitGroup_ExpensesAndPaymentsOnlyFromFirstUser()
        {
            var group = InitGroup();
            group.ExpenseHeaders = new[]
                {
                    new ExpenseHeader
                    {
                        Id = 0,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 30,
                                ExpenseId = 0,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 40,
                                ExpenseId = 0,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 1,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 50,
                                ExpenseId = 1,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 40,
                                ExpenseId = 1,
                                ForUserId = 2
                            }
                        }
                    }

                };
            group.Payments = new List<Payment>
                {
                    new Payment
                    {
                        Id = 0,
                        Amount = 50,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                     new Payment
                    {
                        Id = 1,
                        Amount = 100,
                        IsConfirmed = true,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                      new Payment
                    {
                        Id = 2,
                        Amount = 60,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    }
                };
            return group;
        }
        private Group InitGroup_ExpensesAndUnconfirmedPaymentsOnlyFromFirstUser()
        {
            var group = InitGroup();
            group.ExpenseHeaders = new[]
                {
                    new ExpenseHeader
                    {
                        Id = 0,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 30,
                                ExpenseId = 0,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 40,
                                ExpenseId = 0,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 1,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 50,
                                ExpenseId = 1,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Id = 0,
                                Amount = 40,
                                ExpenseId = 1,
                                ForUserId = 2
                            }
                        }
                    }

                };
            group.Payments = new List<Payment>
                {
                    new Payment
                    {
                        Id = 0,
                        Amount = 50,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                     new Payment
                    {
                        Id = 1,
                        Amount = 100,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                      new Payment
                    {
                        Id = 2,
                        Amount = 60,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    }
                };
            return group;
        }
        private Group InitGroup_NoOneHasDebt()
        {
            var group = InitGroup();
            group.ExpenseHeaders = new[]
                {
                    new ExpenseHeader
                    {
                        Id = 0,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 50,
                                ExpenseId = 0,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 0,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 1,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 50,
                                ExpenseId = 1,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 1,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 2,
                        UserId = 0,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 50,
                                ExpenseId = 2,
                                ForUserId = 1
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 2,
                                ForUserId = 2
                            }
                        }
                    }
                };
            group.Payments = new List<Payment>
                {
                    new Payment
                    {
                        Amount = 50,
                        IsConfirmed = true,
                        FromUserId = 0,
                        ToUserId = 1
                    }
                };
            return group;
        }
        private Group InitGroup_FirstUserHasDebt()
        {
            var group = InitGroup();
            group.ExpenseHeaders = new[]
                {
                    new ExpenseHeader
                    {
                        Id = 0,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 50,
                                ExpenseId = 0,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 0,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 1,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 100,
                                ExpenseId = 1,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 1,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 2,
                        UserId = 0,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 30,
                                ExpenseId = 2,
                                ForUserId = 1
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 2,
                                ForUserId = 2
                            }
                        }
                    },
                     new ExpenseHeader
                    {
                        Id = 3,
                        UserId = 0,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 80,
                                ExpenseId = 3,
                                ForUserId = 1
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 3,
                                ForUserId = 2
                            }
                        }
                    }

                };
            group.Payments = new List<Payment>
                {
                    new Payment
                    {
                        Amount = 60,
                        IsConfirmed = true,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                    new Payment
                    {
                        Amount = 30,
                        IsConfirmed = true,
                        FromUserId = 1,
                        ToUserId = 0
                    },
                     new Payment
                    {
                        Amount = 50,
                        IsConfirmed = false,
                        FromUserId = 1,
                        ToUserId = 0
                    }
                };
            return group;
        }
        private Group InitGroup_SecondUserHasDebt()
        {
            var group = InitGroup();
            group.ExpenseHeaders = new[]
                {
                    new ExpenseHeader
                    {
                        Id = 0,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 70,
                                ExpenseId = 0,
                                ForUserId = 0
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 1,
                        UserId = 1,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 10,
                                ExpenseId = 1,
                                ForUserId = 0
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 1,
                                ForUserId = 2
                            }
                        }
                    },
                    new ExpenseHeader
                    {
                        Id = 2,
                        UserId = 0,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 20,
                                ExpenseId = 2,
                                ForUserId = 1
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 2,
                                ForUserId = 5
                            }
                        }
                    },
                     new ExpenseHeader
                    {
                        Id = 3,
                        UserId = 0,
                        GroupId = 0,
                        ExpenseLines = new List<ExpenseLine>
                        {
                            new ExpenseLine
                            {
                                Amount = 80,
                                ExpenseId = 3,
                                ForUserId = 1
                            },
                            new ExpenseLine
                            {
                                Amount = 40,
                                ExpenseId = 3,
                                ForUserId = 2
                            }
                        }
                    }

                };
            group.Payments = new List<Payment>
                {
                    new Payment
                    {
                        Amount = 30,
                        IsConfirmed = true,
                        FromUserId = 0,
                        ToUserId = 1
                    },
                    new Payment
                    {
                        Amount = 30,
                        IsConfirmed = true,
                        FromUserId = 1,
                        ToUserId = 0
                    },
                     new Payment
                    {
                        Amount = 50,
                        IsConfirmed = false,
                        FromUserId = 1,
                        ToUserId = 0
                    },
                     new Payment
                    {
                        Amount = 50,
                        IsConfirmed = false,
                        FromUserId = 0,
                        ToUserId = 1
                    }
                };
            return group;
        }
    }
}
