﻿namespace SplitwiseApplication.BusinessLogic.Models
{
    public class Debt
    {
        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public decimal Amount { get; set; }
    }
}
