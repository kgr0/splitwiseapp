﻿using System;
namespace SplitwiseApplication.BusinessLogic.Models
{
    public class ErrorResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }

        public ErrorResponse(Exception ex, int statusCode, string message)
        {
            Type = ex.GetType().Name;
            Message = message;
            StatusCode = statusCode;
        }
    }
}
