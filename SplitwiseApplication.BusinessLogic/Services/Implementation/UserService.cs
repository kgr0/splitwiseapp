﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> AuthenticateUserAsync(string login, string password)
        {
            var user = await _userRepository.GetByNameAsync(login);
            var g = Convert.FromBase64String(user.Password);
            var isEqual = VerifyHashedPasswordV2(g, password);
            if(isEqual)
            {
                return user;
            }
            return null;
        }

        public async Task<IEnumerable<Group>> GetUserGroupsAsync(int userId)
        {
            var specification = new GetGroupsOfUserSpecification(userId);
            var user = await _userRepository.GetSingleAsync(specification);
            return user.Groups;
        }

        public async Task<User> RegisterUserAsync(string login, string password)
        {
            var user = new User
            {
                Name = login,
                Password = Convert.ToBase64String(HashPassword(password))
            };
            var insertedUser = await _userRepository.InsertAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync();
            return insertedUser;
        }

        const KeyDerivationPrf Pbkdf2Prf = KeyDerivationPrf.HMACSHA1;
        const int Pbkdf2IterCount = 1000;
        const int Pbkdf2SubkeyLength = 256 / 8;
        const int SaltSize = 128 / 8;

        private byte[] HashPassword(string password)
        {
            var rng = RandomNumberGenerator.Create();
            byte[] salt = new byte[SaltSize];
            rng.GetBytes(salt);
            byte[] subkey = KeyDerivation.Pbkdf2(password, salt, Pbkdf2Prf, Pbkdf2IterCount, Pbkdf2SubkeyLength);

            var outputBytes = new byte[1 + SaltSize + Pbkdf2SubkeyLength];
            outputBytes[0] = 0x00;
            Buffer.BlockCopy(salt, 0, outputBytes, 1, SaltSize);
            Buffer.BlockCopy(subkey, 0, outputBytes, 1 + SaltSize, Pbkdf2SubkeyLength);
            return outputBytes;
        }
        private bool VerifyHashedPasswordV2(byte[] hashedPassword, string password)
        {
            if (hashedPassword.Length != 1 + SaltSize + Pbkdf2SubkeyLength)
            {
                return false;
            }
            byte[] salt = new byte[SaltSize];
            Buffer.BlockCopy(hashedPassword, 1, salt, 0, salt.Length);
            byte[] expectedSubkey = new byte[Pbkdf2SubkeyLength];
            Buffer.BlockCopy(hashedPassword, 1 + salt.Length, expectedSubkey, 0, expectedSubkey.Length);

            byte[] actualSubkey = KeyDerivation.Pbkdf2(password, salt, Pbkdf2Prf, Pbkdf2IterCount, Pbkdf2SubkeyLength);
#if NETSTANDARD2_0 || NET461
            return ByteArraysEqual(actualSubkey, expectedSubkey);
#elif NETCOREAPP
            return CryptographicOperations.FixedTimeEquals(actualSubkey, expectedSubkey);
#else
#error Update target frameworks
#endif
        }
    }
}
