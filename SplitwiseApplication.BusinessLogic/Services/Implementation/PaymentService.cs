﻿using SplitwiseApplication.BusinessLogic.Exceptions;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Implementation
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IGroupRepository _groupRepository;

        public PaymentService(IPaymentRepository paymentRepository, IGroupRepository groupRepository)
        {
            _paymentRepository = paymentRepository;
            _groupRepository = groupRepository;
        }

        public  async Task<IEnumerable<Payment>> GetUserConfirmedPaymentsInGroupAsync(int groupId, int userId)
        {
            var specification = new GetUserConfirmedPaymentsInGroupSpecification(groupId, userId);
            return await _paymentRepository.GetManyAsync(specification);
        }

        public async Task<IEnumerable<Payment>> GetUserPaymentsInGroupAsync(int groupId, int userId)
        {
            var specification = new GetUserPaymentsInGroupSpecification(groupId, userId);
            return await _paymentRepository.GetManyAsync(specification);
        }
        public async Task<Payment> AddPaymentAsync(Payment payment)
        {
            var specification = new GetUsersOfGroupSpecification(payment.GroupId);
            var group = await _groupRepository.GetSingleAsync(specification);
            if (group is null)
            {
                throw new GroupNotFoundException();
            }
            var users = group.Users;

            var user1 = users.Where(x => x.Id == payment.FromUserId);
            var user2 = users.Where(x => x.Id == payment.ToUserId);
            if(user1 is null || user2 is null)
            {
                throw new NotUsersPaymentException();
            }

            var insertedPayment = await _paymentRepository.InsertAsync(payment);
            await _paymentRepository.UnitOfWork.SaveChangesAsync();
            return insertedPayment;
        }

        public async Task ConfirmPaymentAsync(int paymentId, int userId)
        {
            var payment = await _paymentRepository.GetByIdAsync(paymentId);
            if(payment is null)
            {
                throw new PaymentNotFoundException();
            }

            if(payment.IsConfirmed)
            {
                return;
            }
            if(payment.ToUser.Id != userId)
            {
                throw new NotUsersPaymentException();
            }
            payment.IsConfirmed = true;
            await _paymentRepository.UpdateAsync(payment);
            await _paymentRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
