﻿using SplitwiseApplication.BusinessLogic.Exceptions;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Implementation
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;
        public GroupService(IGroupRepository groupRepository, IUserRepository userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }
        public async Task<IEnumerable<User>> GetUsersAsync(int groupId)
        {
            var specification = new GetUsersOfGroupSpecification(groupId);
            var group = await _groupRepository.GetSingleAsync(specification);
            return group.Users;
        }
        public async Task AddUserAsync(int groupId, int userId)
        {
            var group = await _groupRepository.GetByIdAsync(groupId);
            if (group is null)
            {
                throw new GroupNotFoundException();
            }
            var user = await _userRepository.GetByIdAsync(userId);
            if (user is null)
            {
                throw new UserNotFoundException();
            }

            var users = (await GetUsersAsync(groupId)).ToList();
            var userInGroup = users.Where(x => x.Id == userId);
            if (userInGroup.Count() != 0)
                return;

            if (user.Groups is null)
            {
                user.Groups = new List<Group>();
            }
            user.Groups.Add(group);
            await _userRepository.UpdateAsync(user);
            await _userRepository.UnitOfWork.SaveChangesAsync();
            
        }
        public async Task<Group> CreateGroupAsync(string name, int userId)
        {
            var user = await _userRepository.GetByIdAsync(userId);
            var group = new Group
            {
                Name = name,
                Users = new[]
                {
                    user
                }
            };
            var insertedGroup = await _groupRepository.InsertAsync(group);
            await _groupRepository.UnitOfWork.SaveChangesAsync();
            return insertedGroup;
        }
    }
}
