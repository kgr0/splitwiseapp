﻿using SplitwiseApplication.BusinessLogic.Exceptions;
using SplitwiseApplication.BusinessLogic.Models;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.BusinessLogic.Specifications;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Implementation
{
    public class ExpenseService : IExpenseService
    {
        private readonly IExpenseHeaderRepository _expenseHeaderRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IPaymentRepository _paymentRepository;

        public ExpenseService(IExpenseHeaderRepository expenseHeaderRepository, IGroupRepository groupRepository, IPaymentRepository paymentRepository)
        {
            _expenseHeaderRepository = expenseHeaderRepository;
            _groupRepository = groupRepository;
            _paymentRepository = paymentRepository;
        }

        public async Task<IEnumerable<ExpenseHeader>> GetGroupExpensesAsync(int groupId)
        {
            var specification = new GetExpensesOfGroupSpecification(groupId);
            return await _expenseHeaderRepository.GetManyAsync(specification);
        }
        public async Task<ExpenseHeader> AddExpenseAsync(ExpenseHeader expenseHeader)
        {
            var group = await _groupRepository.GetByIdAsync(expenseHeader.GroupId);
            if(group is null)
            {
                throw new GroupNotFoundException();
            }

            var specification = new GetUsersOfGroupSpecification(expenseHeader.GroupId);
            var users = (await _groupRepository.GetManyAsync(specification)).FirstOrDefault().Users.ToList();

            var usersId = users.Select(x => x.Id);
            if (!usersId.Contains(expenseHeader.UserId))
                throw new UserNotInGroupException();

            foreach (var u in expenseHeader.ExpenseLines.Where(x => x.ForUserId != expenseHeader.UserId))
            {
                var userInGroup = usersId.Contains(u.ForUserId);
                if(!userInGroup)
                {
                    throw new UserNotFoundException();
                }
            };

            var insertedExpense = await _expenseHeaderRepository.InsertAsync(expenseHeader);
            await _expenseHeaderRepository.UnitOfWork.SaveChangesAsync();
            return insertedExpense;
        }

        public async Task<Debt> CalculateDebtAsync(int groupId, int fromUserId, int toUserId)
        {
            await GroupExistsAndUsersAreInGroupAsync(groupId, fromUserId, toUserId);

            var debt = new Debt
            {
                FromUserId = fromUserId,
                ToUserId = toUserId,
                Amount = 0
            };
            await CalculateDebtFromExpensesAsync(groupId, fromUserId, toUserId, debt);
            await CalculateDebtFromPaymentsAsync(groupId, fromUserId, toUserId, debt);

            return debt;
        }

        private async Task CalculateDebtFromPaymentsAsync(int groupId, int fromUserId, int toUserId, Debt debt)
        {
            var paymentsOfGroupSpecification = new GetUserConfirmedPaymentsInGroupSpecification(groupId, fromUserId);
            var payments = await _paymentRepository.GetManyAsync(paymentsOfGroupSpecification);

            foreach (var payment in payments)
            {
                if (payment.FromUserId == fromUserId && payment.ToUserId == toUserId)
                {
                    debt.Amount -= payment.Amount;
                }
                if (payment.FromUserId == toUserId && payment.ToUserId == fromUserId)
                {
                    debt.Amount += payment.Amount;
                }
            }
        }

        private async Task CalculateDebtFromExpensesAsync(int groupId, int fromUserId, int toUserId, Debt debt)
        {
            var expensesOfGroupSpecification = new GetExpensesOfGroupSpecification(groupId);
            var expenses = await _expenseHeaderRepository.GetManyAsync(expensesOfGroupSpecification);

            foreach (var expense in expenses)
            {
                if (expense.UserId == fromUserId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.ForUserId == toUserId);
                    if (expenseLine != null)
                    {
                        debt.Amount -= expenseLine.Amount;
                    }
                }

                if (expense.UserId == toUserId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.ForUserId == fromUserId);
                    if (expenseLine != null)
                    {
                        debt.Amount += expenseLine.Amount;
                    }
                }
            }
        }

        private async Task GroupExistsAndUsersAreInGroupAsync(int groupId, int fromUserId, int toUserId)
        {
            var usersInGroupSpecification = new GetUsersOfGroupSpecification(groupId);
            var group = await _groupRepository.GetSingleAsync(usersInGroupSpecification);
            if (group is null)
            {
                throw new GroupNotFoundException();
            }
            var usersIdsOfGroup = group.Users.Select(x => x.Id);
            if (!usersIdsOfGroup.Contains(fromUserId) || !usersIdsOfGroup.Contains(toUserId))
            {
                throw new UserNotInGroupException();
            }
        }
    }
}
