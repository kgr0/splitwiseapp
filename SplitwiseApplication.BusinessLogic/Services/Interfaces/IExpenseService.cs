﻿using SplitwiseApplication.BusinessLogic.Models;
using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IExpenseService
    {
        Task<IEnumerable<ExpenseHeader>> GetGroupExpensesAsync(int groupId);

        Task<ExpenseHeader> AddExpenseAsync(ExpenseHeader expense);
        Task<Debt> CalculateDebtAsync(int groupId, int userId, int forUserId);
    }
}
