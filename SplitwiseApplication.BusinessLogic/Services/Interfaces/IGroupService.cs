﻿using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IGroupService
    {
        Task<IEnumerable<User>> GetUsersAsync(int groupId);

        Task<Group> CreateGroupAsync(string name, int userId);
        Task AddUserAsync(int groupId, int userId);
    }
}
