﻿using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<IEnumerable<Payment>> GetUserPaymentsInGroupAsync(int groupId, int userId);
        Task<IEnumerable<Payment>> GetUserConfirmedPaymentsInGroupAsync(int groupId, int userId);

        Task<Payment> AddPaymentAsync(Payment payment);
        Task ConfirmPaymentAsync(int paymentId, int userId);
    }
}
