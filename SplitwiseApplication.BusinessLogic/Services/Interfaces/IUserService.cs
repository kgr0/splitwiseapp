﻿using SplitwiseApplication.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SplitwiseApplication.BusinessLogic.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<Group>> GetUserGroupsAsync(int userId);
        Task<User> AuthenticateUserAsync(string login, string passsword);
        Task<User> RegisterUserAsync(string login, string password);
    }
}
