﻿using System;
using System.Net;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class GroupNotFoundException : CustomException
    {
        public GroupNotFoundException()
         : base("Group not found", (int)HttpStatusCode.NotFound)
        {

        }
        public GroupNotFoundException(string message)
            : base(message, (int)HttpStatusCode.NotFound)
        {
        }
        public GroupNotFoundException(string message, Exception innerException)
            : base(message, innerException, (int)HttpStatusCode.NotFound)
        {

        }
    }
}
