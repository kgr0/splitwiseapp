﻿using System;
using System.Net;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class UserNotFoundException : CustomException
    {
        public UserNotFoundException()
            : base("User not found", (int)HttpStatusCode.NotFound)
        {
        }
        public UserNotFoundException(string message)
            : base(message, (int)HttpStatusCode.NotFound)
        {
        }
        public UserNotFoundException(string message, Exception innerException)
            : base(message, innerException, (int)HttpStatusCode.NotFound)
        {

        }
    }
}
