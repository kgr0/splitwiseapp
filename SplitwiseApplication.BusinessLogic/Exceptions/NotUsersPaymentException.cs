﻿using System;
using System.Net;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class NotUsersPaymentException : CustomException
    {
        public NotUsersPaymentException()
          : base("User can't access this payment", (int)HttpStatusCode.BadRequest)
        {

        }
        public NotUsersPaymentException(string message)
            : base(message, (int) HttpStatusCode.BadRequest)
        {
        }
        public NotUsersPaymentException(string message, Exception innerException)
            : base(message, innerException, (int)HttpStatusCode.BadRequest)
        {

        }
    }
}
