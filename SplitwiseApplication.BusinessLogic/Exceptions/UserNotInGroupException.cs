﻿using System;
using System.Net;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class UserNotInGroupException : CustomException
    {
        public UserNotInGroupException()
         : base("User can't access this group", (int)HttpStatusCode.BadRequest)
        {

        }
        public UserNotInGroupException(string message)
            : base(message, (int)HttpStatusCode.BadRequest)
        {
        }
        public UserNotInGroupException(string message, Exception innerException)
            : base(message, innerException, (int)HttpStatusCode.BadRequest)
        {

        }
    }
}
