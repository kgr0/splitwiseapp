﻿using System;
using System.Net;

namespace SplitwiseApplication.BusinessLogic.Exceptions
{
    public class PaymentNotFoundException : CustomException
    {
        public PaymentNotFoundException()
           : base("Payment not found", (int)HttpStatusCode.NotFound)
        {

        }
        public PaymentNotFoundException(string message)
            : base(message, (int)HttpStatusCode.NotFound)
        {
        }
        public PaymentNotFoundException(string message, Exception innerException)
            : base(message, innerException, (int)HttpStatusCode.NotFound)
        {

        }
    }
}
