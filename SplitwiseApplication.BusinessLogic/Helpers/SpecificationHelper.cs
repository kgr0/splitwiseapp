﻿using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Helpers
{
    public static class SpecificationHelper
    {
        public static bool IsPaymentOfUserInGroup(Payment payment, int userId, int groupId)
        {
            return payment.GroupId == groupId && (payment.FromUserId == userId || payment.ToUserId == userId);
        }
    }
}
