﻿using Ardalis.Specification;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications
{
    public class GetExpensesOfGroupSpecification : Specification<ExpenseHeader>
    {
        public GetExpensesOfGroupSpecification(int groupId)
        {
            Query.Where(x => x.GroupId == groupId)
                .Include(x => x.ExpenseLines);
        }
    }
}
