﻿using Ardalis.Specification;
using SplitwiseApplication.BusinessLogic.Helpers;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications
{
    public class GetUserConfirmedPaymentsInGroupSpecification : Specification<Payment>
    {
        public GetUserConfirmedPaymentsInGroupSpecification(int groupId, int userId)
        {
           // Query.Where(x => SpecificationHelper.IsPaymentOfUserInGroup(x, userId, groupId)
                  //      && x.IsConfirmed);
            Query.Where(x => x.GroupId == groupId && (x.FromUserId == userId || x.ToUserId == userId) && x.IsConfirmed);
        }
    }
}
