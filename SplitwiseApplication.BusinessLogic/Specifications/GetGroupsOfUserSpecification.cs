﻿using Ardalis.Specification;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SplitwiseApplication.BusinessLogic.Specifications
{
    public class GetGroupsOfUserSpecification : Specification<User>
    {
        public GetGroupsOfUserSpecification(int userId)
        {
            Query.Where(x => x.Id == userId)
                .Include(x => x.Groups);
        }
    }
}
