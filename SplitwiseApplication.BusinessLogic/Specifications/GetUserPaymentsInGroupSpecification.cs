﻿using Ardalis.Specification;
using SplitwiseApplication.BusinessLogic.Helpers;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SplitwiseApplication.BusinessLogic.Specifications
{
    public class GetUserPaymentsInGroupSpecification : Specification<Payment>
    {
        public GetUserPaymentsInGroupSpecification(int groupId, int userId)
        {
            //Query.Where(x => SpecificationHelper.IsPaymentOfUserInGroup(x, userId, groupId));
            Query.Where(x => x.GroupId == groupId && (x.FromUserId == userId || x.ToUserId == userId));
        }
    }
}
