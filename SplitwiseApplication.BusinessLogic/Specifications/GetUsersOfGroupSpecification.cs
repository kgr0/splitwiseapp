﻿using Ardalis.Specification;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.BusinessLogic.Specifications
{
    public class GetUsersOfGroupSpecification : Specification<Group>
    {
        public GetUsersOfGroupSpecification(int groupId)
        {
            Query.Where(x => x.Id == groupId)
                .Include(x => x.Users);
        }
    }
}
