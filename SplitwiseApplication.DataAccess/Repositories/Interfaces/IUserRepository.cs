﻿using SplitwiseApplication.Domain.Models;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User, int>
    {
        Task<User> GetByNameAsync(string name);
    }
}
