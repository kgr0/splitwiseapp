﻿using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SplitwiseApplication.DataAccess.Repositories.Interfaces
{
    public interface IGroupRepository : IRepository<Group, int>
    {
    }
}
