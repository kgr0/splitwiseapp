﻿using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SplitwiseApplication.DataAccess.Repositories.Implementation
{
    public class PaymentRepository : BaseRepository<Payment, int, SplitwiseContext>, IPaymentRepository
    {
        public PaymentRepository(SplitwiseContext context) : base(context)
        {

        }
    }
}
