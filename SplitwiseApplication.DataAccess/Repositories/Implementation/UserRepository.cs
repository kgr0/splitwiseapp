﻿using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;
using System.Threading.Tasks;

namespace SplitwiseApplication.DataAccess.Repositories.Implementation
{
    public class UserRepository : BaseRepository<User, int, SplitwiseContext>, IUserRepository 
    {
        public UserRepository(SplitwiseContext context) : base(context)
        {

        }
        public Task<User> GetByNameAsync(string name)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Name == name);
        }



    }
}
