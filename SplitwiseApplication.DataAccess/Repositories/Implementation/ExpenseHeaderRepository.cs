﻿using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.DataAccess.Repositories.Implementation
{
    public class ExpenseHeaderRepository : BaseRepository<ExpenseHeader, int, SplitwiseContext>, IExpenseHeaderRepository
    {
        public ExpenseHeaderRepository(SplitwiseContext context) : base(context)
        {

        }
    }
}
