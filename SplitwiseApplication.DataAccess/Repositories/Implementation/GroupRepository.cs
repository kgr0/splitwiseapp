﻿using SplitwiseApplication.DataAccess.Context;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.DataAccess.Repositories.Implementation
{
    public class GroupRepository : BaseRepository<Group, int, SplitwiseContext>, IGroupRepository
    {
        public GroupRepository(SplitwiseContext context) : base(context)
        {

        }
    }
}
