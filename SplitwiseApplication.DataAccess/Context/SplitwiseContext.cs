﻿using Microsoft.EntityFrameworkCore;
using SplitwiseApplication.DataAccess.Repositories.Interfaces;
using SplitwiseApplication.Domain.Models;

namespace SplitwiseApplication.DataAccess.Context
{
    public class SplitwiseContext :DbContext, IUnitOfWork
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<ExpenseHeader> ExpenseHeaders { get; set; }
        public DbSet<ExpenseLine> ExpenseLines { get; set; }


        public SplitwiseContext(DbContextOptions<SplitwiseContext> options) :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.FromUser)
                    .WithMany(u => u.PaymentsFromUser)
                    .HasForeignKey(x => x.FromUserId)
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.ToUser)
                    .WithMany(u => u.PaymentsToUser)
                    .HasForeignKey(x => x.ToUserId)
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.Group)
                     .WithMany(u => u.Payments)
                     .HasForeignKey(x => x.GroupId);
                b.Property(x => x.Description)
                      .HasMaxLength(50);
                b.Property(x => x.Time)
                       .HasDefaultValueSql("getutcdate()");
            });

            modelBuilder.Entity<ExpenseLine>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.User)
                     .WithMany(u => u.ExpenseLines)
                     .HasForeignKey(x => x.ForUserId);
                b.HasOne(x => x.ExpenseHeader)
                     .WithMany(u => u.ExpenseLines)
                     .HasForeignKey(x => x.ExpenseId);
            });

            modelBuilder.Entity<ExpenseHeader>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.User)
                     .WithMany(u => u.ExpenseHeaders)
                     .HasForeignKey(x => x.UserId);
                b.HasOne(x => x.Group)
                     .WithMany(u => u.ExpenseHeaders)
                     .HasForeignKey(x => x.GroupId);
                b.Property(x => x.Description)
                     .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name)
                     .HasMaxLength(30);
                b.Property(x => x.Password)
                     .HasMaxLength(100);
            });
            modelBuilder.Entity<Group>(b =>
            {
                b.HasKey(x => x.Id);
                b.Property(x => x.Name)
                     .HasMaxLength(30);
                b.HasMany(x => x.Users)
                .WithMany(x => x.Groups)
                .UsingEntity(x => x.ToTable("GroupUser"));
            });
        }
    }
}
