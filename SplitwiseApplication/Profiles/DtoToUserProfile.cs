﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class DtoToUserProfile :Profile
    {
        public DtoToUserProfile()
        {
            CreateMap<GetUserDto, User>();
            CreateMap<PostUserDto, User>();
        }
    }
}
