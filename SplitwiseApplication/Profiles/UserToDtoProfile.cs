﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class UserToDtoProfile : Profile
    {
        public UserToDtoProfile()
        {
            CreateMap<User, GetUserDto>();
            CreateMap<User, PostUserDto>();
        }
    }
}
