﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class DtoToExpenseProfile : Profile
    {
        public DtoToExpenseProfile()
        {
            CreateMap<GetExpenseDto, ExpenseHeader>();
            CreateMap<GetExpenseLineDto, ExpenseLine>();
            CreateMap<PostExpenseDto, ExpenseHeader>();
            CreateMap<PostExpenseLineDto, ExpenseLine>();
        }
    }
}
