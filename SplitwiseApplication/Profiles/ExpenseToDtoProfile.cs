﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class ExpenseToDtoProfile : Profile
    {
        public ExpenseToDtoProfile()
        {
            CreateMap<ExpenseHeader, GetExpenseDto>();
            CreateMap<ExpenseLine, GetExpenseLineDto>();
            CreateMap<ExpenseHeader, PostExpenseDto>();
            CreateMap<ExpenseLine, PostExpenseLineDto>();
        }
    }
}
