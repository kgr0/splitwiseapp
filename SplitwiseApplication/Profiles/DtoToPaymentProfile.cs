﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class DtoToPaymentProfile : Profile
    {
        public DtoToPaymentProfile()
        {
            CreateMap<GetPaymentDto, Payment>();
            CreateMap<PostPaymentDto, Payment>();
        }
    }
}
