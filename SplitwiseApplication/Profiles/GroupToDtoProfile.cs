﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class GroupToDtoProfile : Profile
    {
        public GroupToDtoProfile()
        {
            CreateMap<Group, GetGroupDto>();
            CreateMap<Group, PostGroupDto>();
        }
    }
}
