﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class PaymentToDtoProfile : Profile
    {
        public PaymentToDtoProfile()
        {
            CreateMap<Payment, GetPaymentDto>();
            CreateMap<Payment, PostPaymentDto>();
        }
    }
}
