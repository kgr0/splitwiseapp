﻿using AutoMapper;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Profiles
{
    public class DtoToGroupProfile : Profile
    {
        public DtoToGroupProfile()
        {
            CreateMap<GetGroupDto, Group>();
            CreateMap<PostGroupDto, Group>();
        }
    }
}
