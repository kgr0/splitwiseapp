﻿using FluentValidation;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Validators
{
    public class PostGroupDtoValidator : AbstractValidator<PostGroupDto>
    {
        public PostGroupDtoValidator()
        {
            RuleFor(x => x.Name)
               .Cascade(CascadeMode.Stop)
               .NotEmpty()
               .Length(1, 30);

        }
    }
}
