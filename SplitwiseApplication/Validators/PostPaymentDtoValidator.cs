﻿using FluentValidation;
using SplitwiseApplication.Dto;

namespace SplitwiseApplication.Validators
{
    public class PostPaymentDtoValidator : AbstractValidator<PostPaymentDto>
    {
        public PostPaymentDtoValidator()
        {
            RuleFor(x => x.Description)
                .Cascade(CascadeMode.Stop)
                .NotEmpty()
                .Length(1, 50);

        }
    }
}
