﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SplitwiseApplication.Controllers
{
    [Route("api/payments")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;

        public PaymentController(IPaymentService paymentService, IMapper mapper)
        {
            _paymentService = paymentService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<GetPaymentDto>>> GetUsersPaymentsInGroup(int groupId)
        {
            var payments = await _paymentService.GetUserPaymentsInGroupAsync(groupId, UserId);
            var result = _mapper.Map<IEnumerable<GetPaymentDto>>(payments);
            return Ok(result);
        }

        [HttpGet("confirmed/{groupId}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<GetPaymentDto>>> GetUsersConfirmedPaymentsInGroup(int groupId)
        {
            var payments = await _paymentService.GetUserConfirmedPaymentsInGroupAsync(groupId, UserId);
            var result = _mapper.Map<IEnumerable<GetPaymentDto>>(payments);
            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<GetPaymentDto>> AddPayment([FromBody] PostPaymentDto paymentDto)
        {
            var payment = _mapper.Map <Payment> (paymentDto);
            payment.FromUserId = UserId;
            var insertedPayment = await _paymentService.AddPaymentAsync(payment);
            var result = _mapper.Map<GetPaymentDto>(insertedPayment);
            return result;
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> ConfirmPaymentAsync(int id)
        {
            await _paymentService.ConfirmPaymentAsync(id, UserId);
            return Ok();
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
