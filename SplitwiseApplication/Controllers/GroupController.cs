﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SplitwiseApplication.BusinessLogic.Models;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SplitwiseApplication.Controllers
{
    [Route("api/groups")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;
        private readonly IExpenseService _expenseService;
        private readonly IMapper _mapper;

        public GroupController(IGroupService groupService, IExpenseService expenseService,  IMapper mapper)
        {
            _groupService = groupService;
            _expenseService = expenseService;
            _mapper = mapper;
        }

        [HttpGet ("{groupId}")]
        public async Task<ActionResult<IEnumerable<GetUserDto>>> GetUsers(int groupId)
        {
            var users = await _groupService.GetUsersAsync(groupId);
            var result = _mapper.Map<IEnumerable<GetUserDto>>(users);
            return Ok(result);
        }

        [HttpGet("{groupId}/debts/")]
        [Authorize]
        public async Task<ActionResult<Debt>> GetDebt(int groupId, int forUserId)
        {
            var debt = await _expenseService.CalculateDebtAsync(groupId, UserId, forUserId);
            return Ok(debt);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<GetGroupDto>> CreateGroup(PostGroupDto groupDto)
        {
            var createdGroup = await _groupService.CreateGroupAsync(groupDto.Name, UserId);
            var result = _mapper.Map<GetGroupDto>(createdGroup);
            return result;
        }

        [HttpPost("{groupId}/users/{userId}")]
        [Authorize]
        public async Task<ActionResult> AddUserToGroupAsync(int groupId, int userId)
        {
            await _groupService.AddUserAsync(groupId, userId);
            return Ok();
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
