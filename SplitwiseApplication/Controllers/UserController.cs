﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.Dto;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SplitwiseApplication.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<GetUserDto>> RegisterUser([FromBody] PostUserDto userDto)
        {
            var insertedUser = await _userService.RegisterUserAsync(userDto.Name, userDto.Password);
            var result = _mapper.Map<GetUserDto>(insertedUser);
            return result;
        }

        [HttpGet("groups")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<GetGroupDto>>> GetGroups()
        {
            var groups = await _userService.GetUserGroupsAsync(UserId);
            var result = _mapper.Map<IEnumerable<GetGroupDto>>(groups);
            return Ok(result);
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
