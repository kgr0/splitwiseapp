﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SplitwiseApplication.BusinessLogic.Models;
using SplitwiseApplication.BusinessLogic.Services.Interfaces;
using SplitwiseApplication.Domain.Models;
using SplitwiseApplication.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SplitwiseApplication.Controllers
{
    [Route("api/expenses")]
    [ApiController]
    public class ExpenseController : ControllerBase
    {
        private readonly IExpenseService _expenseService;
        private readonly IMapper _mapper;
        public ExpenseController(IExpenseService expenseService, IMapper mapper)
        {
            _expenseService = expenseService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<GetExpenseDto>>> GetGroupExpensesAsync(int groupId)
        {
            var expenses = await _expenseService.GetGroupExpensesAsync(groupId);
            var result = _mapper.Map<IEnumerable<GetExpenseDto>>(expenses);
            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<GetExpenseDto>> AddExpenseAsync([FromBody] PostExpenseDto expenseDto)
        {
            var expense = _mapper.Map<ExpenseHeader>(expenseDto);
            expense.UserId = UserId;
            var insertedExpense = await _expenseService.AddExpenseAsync(expense);
            var result = _mapper.Map<GetExpenseDto>(insertedExpense);
            return result;
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
