﻿using SplitwiseApplication.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SplitwiseApplication.Dto
{
    public class PostExpenseDto
    {
        public string Description { get; set; }
        public int GroupId { get; set; }

        public ICollection<PostExpenseLineDto> ExpenseLines { get; set; }
    }
}
