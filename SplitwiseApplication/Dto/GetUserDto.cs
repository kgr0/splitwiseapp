﻿namespace SplitwiseApplication.Dto
{
    public class GetUserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
