﻿namespace SplitwiseApplication.Dto
{
    public class PostExpenseLineDto
    {
        public decimal Amount { get; set; }
        public int ForUserId { get; set; }
    }
}
