﻿using System;
using System.Collections.Generic;

namespace SplitwiseApplication.Dto
{
    public class GetExpenseDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }

        public int UserId { get; set; }
        public int GroupId { get; set; }

        public ICollection<GetExpenseLineDto> ExpenseLines { get; set; }
    }
}
