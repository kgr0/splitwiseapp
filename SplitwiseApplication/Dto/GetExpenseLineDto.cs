﻿namespace SplitwiseApplication.Dto
{
    public class GetExpenseLineDto
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }

        public int ExpenseId { get; set; }
        public int ForUserId { get; set; }
    }
}
