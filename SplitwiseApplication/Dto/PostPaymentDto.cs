﻿namespace SplitwiseApplication.Dto
{
    public class PostPaymentDto
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public int ToUserId { get; set; }
        public int GroupId { get; set; }
    }
}
