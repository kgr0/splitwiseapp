﻿namespace SplitwiseApplication.Dto
{
    public class PostUserDto
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
