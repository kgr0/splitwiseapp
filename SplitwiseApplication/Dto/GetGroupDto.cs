﻿namespace SplitwiseApplication.Dto
{
    public class GetGroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
