﻿using System;

namespace SplitwiseApplication.Dto
{
    public class GetPaymentDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
        public bool IsConfirmed { get; set; }

        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public int GroupId { get; set; }

    }
}
